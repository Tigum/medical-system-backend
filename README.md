This application was developed in Flask (Python).

## Instructions to run application locally

1. Install postgres database. This can be done by installing pgAdmin 4. (https://www.pgadmin.org/download/)
2. Start pgAdmin 4 and create two databases called "puc" and "puc_test"
3. Create a virtual environment using "pipenv" (https://pypi.org/project/pipenv/). This can be done by opening the terminal and typing "pipenv --three" in the project's folder. (make sure env variables are configured)
3. Run the following commands in the project's root folder, 'python manage.py db init', 'python manage.py db migrate', 'python manage.py db upgrade'. This will update postgres' tables.
4. Access virtual environment by typing "pipenv shell" in the terminal, in the project's folder.
5. Run application using "python run.py". Application will be available in http://0.0.0.0:5000
6. Unit tests can be run by running command "pytest" in the terminal.