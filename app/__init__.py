# src/app.py

import os

from flask import Flask
from flask_cors import CORS

from .config import app_config
from .models import db, bcrypt

from .views.ServiceStationView import service_station_api as service_station_blueprint
from .views.AddressView import address_api as address_blueprint
from .views.PacientView import pacient_api as pacient_blueprint
from .views.UserView import user_api as user_blueprint
from .views.ExamView import exam_api as exam_blueprint


def create_app(env_name=os.getenv("FLASK_ENV", "production")):
    # app initiliazation
    app = Flask(__name__)

    if env_name == "development":
        CORS(app, resources={r"/*": {"origins": "*"}})

    app.config.from_object(app_config[env_name])

    # initializing bcrypt and db
    bcrypt.init_app(app)
    db.init_app(app)

    app.register_blueprint(user_blueprint, url_prefix="/api/v1/user")
    app.register_blueprint(pacient_blueprint, url_prefix="/api/v1/pacient")
    app.register_blueprint(service_station_blueprint, url_prefix="/api/v1/service_station")
    app.register_blueprint(address_blueprint, url_prefix="/api/v1/address")
    app.register_blueprint(exam_blueprint, url_prefix="/api/v1/exam")

    return app
