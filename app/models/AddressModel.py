from marshmallow import fields, Schema
import datetime
from . import db


class AddressModel(db.Model):

    __tablename__ = "addresses"

    id = db.Column(db.Integer, primary_key=True)
    street_name = db.Column(db.String(), nullable=False)
    zipcode = db.Column(db.String(), nullable=False)
    number = db.Column(db.String(), nullable=False)
    city = db.Column(db.String(), nullable=False)
    state = db.Column(db.String(), nullable=False)
    country = db.Column(db.String(), nullable=False)
    created_at = db.Column(db.DateTime)
    modified_at = db.Column(db.DateTime)

    def __init__(self, data):
        self.street_name = data.get("street_name")
        self.zipcode = data.get("zipcode")
        self.number = data.get("number")
        self.city = data.get("city")
        self.state = data.get("state")
        self.country = data.get("country")
        self.created_at = datetime.datetime.utcnow()
        self.modified_at = datetime.datetime.utcnow()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        ser_address = address_schema.dump(self)
        for key in data:
            if ser_address[key] != data[key]:
                setattr(self, key, data[key])
        self.modified_at = datetime.datetime.utcnow()
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_all_addresses():
        return AddressModel.query.all()

    @staticmethod
    def get_one_address(id):
        return AddressModel.query.get(id)

class AddressSchema(Schema):
    id = fields.Int(dump_only=True)
    street_name = fields.Str(required=True)
    zipcode = fields.Str(required=True)
    number = fields.Str(required=True)
    city = fields.Str(required=True)
    state = fields.Str(required=True)
    country = fields.Str(required=True)
    created_at = fields.DateTime(dump_only=True)
    modified_at = fields.DateTime(dump_only=True)


address_schema = AddressSchema()