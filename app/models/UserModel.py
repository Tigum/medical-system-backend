from marshmallow import fields, Schema
from .AddressModel import AddressSchema, AddressModel
import datetime
from . import db, bcrypt


class UserModel(db.Model):

    __tablename__ = "users"

    #ROLES CAN BE:
    # - master
    # - admin
    # - doctor

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(), nullable=False)
    surname = db.Column(db.String(), nullable=False)
    email = db.Column(db.String(), nullable=False)
    password = db.Column(db.String(), nullable=False)
    phone = db.Column(db.String(), nullable=False)
    address = db.Column(db.Integer, db.ForeignKey("addresses.id"), nullable=False)
    role = db.Column(db.String(), nullable=False)
    doctor_type = db.Column(db.String(), nullable=False)
    start_at = db.Column(db.String(), nullable=False)
    end_at = db.Column(db.String(), nullable=False)
    created_at = db.Column(db.DateTime)
    modified_at = db.Column(db.DateTime)
    

    def __init__(self, data):
        self.name = data.get("name")
        self.surname = data.get("surname")
        self.email = data.get("email")
        self.password = self.__generate_hash(data.get('password'))
        self.phone = data.get("phone")
        self.address = data.get("address")
        self.role = data.get("role")
        self.start_at = data.get("start_at")
        self.end_at = data.get("end_at")
        self.created_at = datetime.datetime.utcnow()
        self.modified_at = datetime.datetime.utcnow()

        if data.get("doctor_type"):
            self.doctor_type = data.get("doctor_type")
        else:
            self.doctor_type = 'N/A'


    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, item in data.items():
            if key == 'password':
                self.password = self.__generate_hash(item)
                setattr(self, key, self.password)
            else:
                setattr(self, key, item)
        self.modified_at = datetime.datetime.utcnow()
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_all_users():
        return UserModel.query.all()

    @staticmethod
    def get_one_user_by_id(id):
        return UserModel.query.get(id)
    
    @staticmethod
    def get_one_user_by_email(email):
        return UserModel.query.filter_by(email=email).first()

    def __generate_hash(self, password):
        return bcrypt.generate_password_hash(password, rounds=10).decode('utf-8')
    
    def check_hash(self, password):
        return bcrypt.check_password_hash(self.password, password)


class UserSchema(Schema):
    id = fields.Int()
    name = fields.Str(required=True)
    surname = fields.Str(required=True)
    email = fields.Email(required=True)
    password = fields.Str(required=False, load_only=True)
    phone = fields.Str(required=True)
    address = fields.Int(required=True)
    role = fields.Str(required=True)
    start_at = fields.Str(required=True)
    end_at = fields.Str(required=True)
    user_id = fields.Int(required=False, load_only=True)
    addresses = fields.Nested(AddressSchema, dump_only=True, required=False)
    doctor_type = fields.Str(required=False)
    created_at = fields.DateTime(dump_only=True)
    modified_at = fields.DateTime(dump_only=True)



user_schema = UserSchema()