from marshmallow import fields, Schema
from .AddressModel import AddressSchema
import datetime
from . import db


class ExamModel(db.Model):

    __tablename__ = "exams"

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(), nullable=False)
    description = db.Column(db.String(), nullable=False)
    pacient = db.Column(db.Integer, db.ForeignKey("pacients.id"), nullable=False)
    is_procedure = db.Column(db.Boolean)
    created_at = db.Column(db.DateTime)
    modified_at = db.Column(db.DateTime)
    

    def __init__(self, data):
        self.title = data.get("title")
        self.description = data.get("description")
        self.pacient = data.get('pacient')
        self.is_procedure = data.get('is_procedure')
        self.created_at = datetime.datetime.utcnow()
        self.modified_at = datetime.datetime.utcnow()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        ser_exam = exam_schema.dump(self)
        for key in data:
            if ser_exam[key] != data[key]:
                setattr(self, key, data[key])
        self.modified_at = datetime.datetime.utcnow()
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_all_exams():
        return ExamModel.query.all()

    @staticmethod
    def get_one_exam_by_id(id):
        return ExamModel.query.get(id)
    
    @staticmethod
    def get_all_exams_by_pacient_id(pacient_id):
        return ExamModel.query.filter_by(pacient=pacient_id).all()

class ExamSchema(Schema):
    id = fields.Int(dump_only=True)
    title = fields.Str(required=True)
    description = fields.Str(required=True)
    pacient = fields.Int(required=True)
    is_procedure = fields.Boolean(required=True)
    created_at = fields.DateTime(dump_only=True)
    modified_at = fields.DateTime(dump_only=True)



exam_schema = ExamSchema()