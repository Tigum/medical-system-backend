from marshmallow import fields, Schema
from .AddressModel import AddressSchema
from .ExamModel import ExamSchema
import datetime
from . import db


class PacientModel(db.Model):

    __tablename__ = "pacients"

    #ROLES CAN BE:
    # - master
    # - admin
    # - doctor
    # - pacient

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(), nullable=False)
    surname = db.Column(db.String(), nullable=False)
    email = db.Column(db.String(), nullable=False)
    phone = db.Column(db.String(), nullable=False)
    address = db.Column(db.Integer, db.ForeignKey("addresses.id"), nullable=False)
    exams = db.relationship('ExamModel', backref='pacients_exams', cascade='all, delete')
    created_at = db.Column(db.DateTime)
    modified_at = db.Column(db.DateTime)
    

    def __init__(self, data):
        self.name = data.get("name")
        self.surname = data.get("surname")
        self.email = data.get("email")
        self.phone = data.get("phone")
        self.address = data.get("address")
        self.created_at = datetime.datetime.utcnow()
        self.modified_at = datetime.datetime.utcnow()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        ser_pacient = pacient_schema.dump(self)
        del data['pacient_id']
        for key in data:
            if ser_pacient[key] != data[key]:
                setattr(self, key, data[key])
        self.modified_at = datetime.datetime.utcnow()
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_all_pacients():
        return PacientModel.query.all()

    @staticmethod
    def get_one_pacient_by_id(id):
        return PacientModel.query.get(id)
    
    @staticmethod
    def get_one_pacient_by_email(email):
        return PacientModel.query.filter_by(email=email).first()

class PacientSchema(Schema):
    id = fields.Int(dump_only=True)
    name = fields.Str(required=True)
    surname = fields.Str(required=True)
    email = fields.Email(required=True)
    phone = fields.Str(required=True)
    address = fields.Int(required=True)
    pacient_id = fields.Int(required=False, load_only=True)
    addresses = fields.Nested(AddressSchema ,many=True, dump_only=True)
    exams = fields.Nested(ExamSchema ,many=True, dump_only=True)
    created_at = fields.DateTime(dump_only=True)
    modified_at = fields.DateTime(dump_only=True)



pacient_schema = PacientSchema()