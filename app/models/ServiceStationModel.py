from marshmallow import fields, Schema
from .PacientModel import PacientModel, PacientSchema
from .UserModel import UserSchema
from .AddressModel import AddressSchema
import datetime
from . import db

station_users = db.Table('station_users',
    db.Column('station_id', db.Integer, db.ForeignKey('service_stations.id'), primary_key=True),
    db.Column('user_id', db.Integer, db.ForeignKey('users.id'), primary_key=True)
)

station_pacients = db.Table('station_pacients',
    db.Column('station_id', db.Integer, db.ForeignKey('service_stations.id'), primary_key=True),
    db.Column('pacient_id', db.Integer, db.ForeignKey('pacients.id'), primary_key=True)
)

class ServiceStationModel(db.Model):

    __tablename__ = "service_stations"

    #STATION TYPES CAN BE:
    # - hospital
    # - lab
    # - health_station
    # - clinic

    id = db.Column(db.Integer, primary_key=True)
    label = db.Column(db.String(), nullable=False)
    station_type = db.Column(db.String(), nullable=False)
    private = db.Column(db.Boolean(), nullable=False)
    address = db.Column(db.Integer, db.ForeignKey("addresses.id"), nullable=False)
    users = db.relationship('UserModel', secondary=station_users, lazy='subquery', backref=db.backref('service_stations', lazy=True))
    pacients = db.relationship('PacientModel', secondary=station_pacients, lazy='subquery', backref=db.backref('service_stations', lazy=True))
    created_at = db.Column(db.DateTime)
    modified_at = db.Column(db.DateTime)

    def __init__(self, data):
        self.label = data.get("label")
        self.station_type = data.get("station_type")
        self.private = data.get("private")
        self.address = data.get("address")
        self.created_at = datetime.datetime.utcnow()
        self.modified_at = datetime.datetime.utcnow()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        ser_service_station = service_station_schema.dump(self)
        for key in data:
            if ser_service_station[key] != data[key]:
                setattr(self, key, data[key])
        self.modified_at = datetime.datetime.utcnow()
        db.session.commit()
    
    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def add_user_to_station(self, user):
        self.users.append(user)
        self.modified_at = datetime.datetime.utcnow()
        db.session.commit()
    
    def remove_user_to_station(self, user):
        self.users.remove(user)
        self.modified_at = datetime.datetime.utcnow()
        db.session.commit()

    @staticmethod
    def get_all_service_stations():
        return ServiceStationModel.query.all()

    @staticmethod
    def get_one_service_station(id):
        return ServiceStationModel.query.get(id)

    @staticmethod
    def filter_by_type(station_type):
        return ServiceStationModel.query.filter_by(station_type=station_type).all()

    @staticmethod
    def filter_by_private(private):
        return ServiceStationModel.query.filter_by(private=private).all()

class ServiceStationSchema(Schema):
    id = fields.Int(dump_only=True)
    label = fields.Str(required=True)
    station_type = fields.Str(required=True)
    private = fields.Boolean(required=True)
    address = fields.Int(required=True)
    pacients = fields.Nested(PacientSchema, required=False, many=True)
    users = fields.Nested(UserSchema, required=False, many=True, dump_only=True)
    addresses = fields.Nested(AddressSchema ,many=True, dump_only=True)
    created_at = fields.DateTime(dump_only=True)
    modified_at = fields.DateTime(dump_only=True)


service_station_schema = ServiceStationSchema()