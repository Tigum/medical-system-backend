from flask import request, json, Response, Blueprint, g
from ..models.AddressModel import AddressModel, address_schema
from ..utils.CustomResponse import (
    CREATION_CODE,
    ERROR_CODE,
    custom_response,
    DELETED_CODE,
    SUCCESS_CODE,
)

address_api = Blueprint("address_api", __name__)


@address_api.route("/", methods=["POST"])
def create():
    req_data = request.get_json()
    address_info =  address_schema.load(req_data)
    address = AddressModel(address_info)
    address.save()
    ser_data = address_schema.dump(address)
    return custom_response("success", ser_data, CREATION_CODE)

@address_api.route("/<address_id>", methods=["GET"])
def get_address_by_id(address_id):
    address = AddressModel.get_one_address(address_id)
    if not address:
        return custom_response('Address does not exist', {}, ERROR_CODE)
    ser_data = address_schema.dump(address)
    return custom_response("success", ser_data, SUCCESS_CODE)