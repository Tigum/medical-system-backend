from flask import request, json, Response, Blueprint, g
from ..models.PacientModel import PacientModel, pacient_schema
from ..utils.CustomResponse import (
    CREATION_CODE,
    ERROR_CODE,
    custom_response,
    DELETED_CODE,
    SUCCESS_CODE,
)

pacient_api = Blueprint("pacient_api", __name__)


@pacient_api.route("/", methods=["POST"])
def create():
    req_data = request.get_json()
    pacient_info =  pacient_schema.load(req_data)
    pacient = PacientModel(pacient_info)
    pacient.save()
    ser_data = pacient_schema.dump(pacient)
    return custom_response("success", ser_data, CREATION_CODE)


@pacient_api.route("/<pacient_id>", methods=["GET"])
def fetch_pacient_by_id(pacient_id):
    pacient = PacientModel.get_one_pacient_by_id(pacient_id)
    if not pacient:
        return custom_response('No pacient found', {}, ERROR_CODE)
    ser_data = pacient_schema.dump(pacient)
    return custom_response("success", ser_data, SUCCESS_CODE)

@pacient_api.route("/", methods=["GET"])
def fetch_all_pacients():
    pacients = PacientModel.get_all_pacients()
    ser_data = pacient_schema.dump(pacients, many=True)
    return custom_response("success", ser_data, SUCCESS_CODE)


@pacient_api.route("/", methods=["PUT"])
def edit_pacient_info():
    req_data = request.get_json()
    data = pacient_schema.load(req_data, partial=True)
    pacient = PacientModel.get_one_pacient_by_id(data.get('pacient_id'))
    if not pacient:
        return custom_response('No pacient found', {}, ERROR_CODE)
    pacient.update(data)
    ser_data = pacient_schema.dump(pacient)
    return custom_response("success", ser_data, SUCCESS_CODE)


@pacient_api.route("/<pacient_id>", methods=["DELETE"])
def delete_pacient_by_id(pacient_id):
    pacient = PacientModel.get_one_pacient_by_id(pacient_id)
    if not pacient:
        return custom_response('No pacient found', {}, ERROR_CODE)
    pacient.delete()
    return custom_response("success", {}, DELETED_CODE)