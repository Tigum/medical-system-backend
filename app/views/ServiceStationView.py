from flask import request, json, Response, Blueprint, g
from ..models.ServiceStationModel import service_station_schema, ServiceStationModel
from ..models.UserModel import UserModel, user_schema
from ..utils.CustomResponse import (
    CREATION_CODE,
    ERROR_CODE,
    custom_response,
    DELETED_CODE,
    SUCCESS_CODE,
)

service_station_api = Blueprint("service_station_api", __name__)


@service_station_api.route("/", methods=["POST"])
def create():
    req_data = request.get_json()
    label = req_data.get("label")
    station_type = req_data.get("station_type")
    private = req_data.get("private")

    if not (label or station_type or private):
        return custom_response("There are information missing, please review and try again", [], ERROR_CODE)

    service_station_info = service_station_schema.load(req_data)
    service_station = ServiceStationModel(service_station_info)
    service_station.save()

    ser_data = service_station_schema.dump(service_station)

    return custom_response("success", ser_data, CREATION_CODE)


@service_station_api.route("/", methods=["GET"])
def fetch_all_service_stations():
    service_stations = ServiceStationModel.get_all_service_stations()
    ser_data = service_station_schema.dump(service_stations, many=True)
    return custom_response("success", ser_data, SUCCESS_CODE)


@service_station_api.route("/<station_id>", methods=["GET"])
def fetch_one_service_stations(station_id):
    service_station = ServiceStationModel.get_one_service_station(station_id)
    if not service_station:
        return custom_response("Service station ID does not exist", [], ERROR_CODE)
    ser_data = service_station_schema.dump(service_station)
    return custom_response("success", ser_data, SUCCESS_CODE)

@service_station_api.route("/<station_id>", methods=["DELETE"])
def delete(station_id):
    service_station = ServiceStationModel.get_one_service_station(station_id)
    if not service_station:
        return custom_response("Service station ID does not exist", [], ERROR_CODE)
    service_station.delete()
    return custom_response("success", {}, DELETED_CODE)

@service_station_api.route("/user", methods=["PUT"])
def associate_user_to_station():
    req_data = request.get_json()
    station_id = req_data.get('station_id')
    user_id = req_data.get('user_id')
    
    if not station_id:
        return custom_response("Please provide station ID", [], ERROR_CODE)
    
    if not user_id:
        return custom_response("Please provide user ID", [], ERROR_CODE)

    user = UserModel.get_one_user_by_id(user_id)
    if not user:
        return custom_response("User ID provided is invalid", [], ERROR_CODE)

    service_station = ServiceStationModel.get_one_service_station(station_id)
    if not service_station:
        return custom_response("Station ID provided is invalid", [], ERROR_CODE)

    service_station.add_user_to_station(user)
    service_station = ServiceStationModel.get_one_service_station(station_id)
    ser_station = service_station_schema.dump(service_station)
    return custom_response("success", ser_station, SUCCESS_CODE)


@service_station_api.route("/user/remove/<user_id>/<station_id>", methods=["PUT"])
def dessociate_user_to_station(user_id, station_id):
    if not station_id:
        return custom_response("Please provide station ID", [], ERROR_CODE)

    if not user_id:
        return custom_response("Please provide user ID", [], ERROR_CODE)

    user = UserModel.get_one_user_by_id(user_id)
    if not user:
        return custom_response("User ID provided is invalid", [], ERROR_CODE)

    service_station = ServiceStationModel.get_one_service_station(station_id)
    if not service_station:
        return custom_response("Station ID provided is invalid", [], ERROR_CODE)

    if user in service_station.users:
        service_station.remove_user_to_station(user)
        service_station = ServiceStationModel.get_one_service_station(station_id)
        ser_station = service_station_schema.dump(service_station)
        return custom_response("success", ser_station, SUCCESS_CODE)

    return custom_response("User is already not associated to this service station", {}, ERROR_CODE)
