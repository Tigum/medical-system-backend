from flask import request, json, Response, Blueprint, g
from ..models.UserModel import UserModel, user_schema
from ..models.AddressModel import AddressModel, address_schema
from ..shared.Authentication import Auth
from ..utils.CustomResponse import (
    CREATION_CODE,
    ERROR_CODE,
    custom_response,
    DELETED_CODE,
    SUCCESS_CODE,
)

user_api = Blueprint("user_api", __name__)


@user_api.route("/", methods=["POST"])
def create():
    req_data = request.get_json()
    user_info =  user_schema.load(req_data)

    user_exists = UserModel.get_one_user_by_email(user_info.get('email'))
    if user_exists:
        return custom_response("User already exists", {}, ERROR_CODE)

    user = UserModel(user_info)
    user.save()
    ser_data = user_schema.dump(user)
    ser_data['token'] = Auth.generate_token(ser_data.get('id'))
    return custom_response("success", ser_data, CREATION_CODE)

@user_api.route("/login", methods=["POST"])
def login():
    req_data = request.get_json()
    user_info =  user_schema.load(req_data, partial=True)
    user = UserModel.get_one_user_by_email(user_info.get('email'))
    if not user:
        return custom_response("User does not exists", {}, ERROR_CODE)

    if not user.check_hash(user_info.get('password')):
        return custom_response("Invalid credentials", {}, ERROR_CODE)

    ser_data = user_schema.dump(user)
    ser_data['token'] = Auth.generate_token(ser_data.get('id'))

    address = AddressModel.get_one_address(ser_data['address'])
    if address:
        ser_address = address_schema.dump(address)
        ser_data['address'] = ser_address
    
    return custom_response("success", ser_data, SUCCESS_CODE)


@user_api.route("/", methods=["GET"])
def fetch_all_users():
    users = UserModel.get_all_users()
    ser_data = user_schema.dump(users, many=True)
    return custom_response("success", ser_data, SUCCESS_CODE)


@user_api.route("/<user_id>", methods=["GET"])
def fetch_user_by_id(user_id):
    user = UserModel.get_one_user_by_id(user_id)
    if not user:
        return custom_response('No user found', {}, ERROR_CODE)
    ser_data = user_schema.dump(user)
    return custom_response("success", ser_data, SUCCESS_CODE)


@user_api.route("/", methods=["PUT"])
def edit_user_info():
    req_data = request.get_json()
    data = user_schema.load(req_data, partial=True)
    user = UserModel.get_one_user_by_id(data.get('user_id'))
    if not user:
        return custom_response('No user found', {}, ERROR_CODE)
    user.update(data)
    ser_data = user_schema.dump(user)
    return custom_response("success", ser_data, SUCCESS_CODE)


@user_api.route("/<user_id>", methods=["DELETE"])
def delete_user_by_id(user_id):
    user = UserModel.get_one_user_by_id(user_id)
    if not user:
        return custom_response('No user found', {}, ERROR_CODE)
    ser_data = user_schema.dump(user)
    user.delete()
    return custom_response("success", {}, DELETED_CODE)


@user_api.route("/token", methods=["POST"])
def check_token():
    req_data = request.get_json()
    token = req_data.get('token')

    token_exists = Auth.decode_token(token)

    if token_exists['error']:
        return custom_response('Token is invalid', {}, ERROR_CODE)
        
    if token_exists['data']['user_id']:
        user = UserModel.get_one_user_by_id(token_exists['data']['user_id'])
        if not user:
            return custom_response('No user found', {}, ERROR_CODE)
        ser_user = user_schema.dump(user)
        return custom_response("success", ser_user, SUCCESS_CODE)
    
    return custom_response('Error authenticating token', {}, ERROR_CODE)
    
