from flask import request, json, Response, Blueprint, g
from ..models.ExamModel import ExamModel, exam_schema
from ..models.PacientModel import PacientModel
from ..utils.CustomResponse import (
    CREATION_CODE,
    ERROR_CODE,
    custom_response,
    DELETED_CODE,
    SUCCESS_CODE,
)

exam_api = Blueprint("exam_api", __name__)


@exam_api.route("/", methods=["POST"])
def create():
    req_data = request.get_json()
    exam_info =  exam_schema.load(req_data)
    exam = ExamModel(exam_info)
    exam.save()
    ser_data = exam_schema.dump(exam)
    return custom_response("success", ser_data, CREATION_CODE)


@exam_api.route("/<pacient_id>", methods=["GET"])
def get_pacients_exams(pacient_id):
    pacient = PacientModel.get_one_pacient_by_id(pacient_id)
    if not pacient:
        return custom_response('Pacient does not exist', {}, ERROR_CODE)

    exams = ExamModel.get_all_exams_by_pacient_id(pacient_id)
    ser_data = exam_schema.dump(exams, many=True)
    return custom_response("success", ser_data, SUCCESS_CODE)

@exam_api.route("/", methods=["GET"])
def get_all_exams():
    exams = ExamModel.get_all_exams()
    ser_data = exam_schema.dump(exams, many=True)
    return custom_response("success", ser_data, SUCCESS_CODE)


@exam_api.route("/<exam_id>", methods=["DELETE"])
def delete(exam_id):
    if not exam_id:
        return custom_response('Please provide exam ID', {}, ERROR_CODE)
    exam = ExamModel.get_one_exam_by_id(exam_id)
    if not exam:
        return custom_response('Exam does not exist', {}, ERROR_CODE)
    exam.delete()
    return custom_response("success", {}, DELETED_CODE)