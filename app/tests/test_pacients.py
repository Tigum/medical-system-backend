import unittest
import os
import json
from app import create_app, db
from ..utils.CustomResponse import ERROR_CODE, SUCCESS_CODE, CREATION_CODE, DELETED_CODE


class PacientsTest(unittest.TestCase):
    def setUp(self):

        self.app = create_app("testing")
        self.client = self.app.test_client
        self.address = {
            "street_name": 'Rua X',
            "zipcode": '12345-67',
            "number": '100',
            "city": 'Campinas',
            "state": 'São Paulo',
            "country": 'Brasil'
        }
        self.create_pacient = {
            "name": "Pacient name",
            "surname": "Pacient surname",
            "email": "pacient@gmail.com",
            "phone": "19 00000-0000"
        }
        self.update_pacient = {
            "name": "João",
            "surname": "Silva",
            "email": "joao@gmail.com",
            "phone": "19 00000-0001"
        }

        with self.app.app_context():
            # create all tables
            db.create_all()

    def test_pacient_creation(self):
        """ test pacient creation successfully """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_pacient["address"] = json_data["data"]["id"]

        #CREATE PACIENT
        res = self.client().post(
            "/api/v1/pacient/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_pacient),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)


    def test_fetch_pacient_by_id(self):
        """ test fetch pacient by id successfully """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_pacient["address"] = json_data["data"]["id"]

        #CREATE PACIENT
        res = self.client().post(
            "/api/v1/pacient/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_pacient),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        #FETCH CREATED PACIENT
        res = self.client().get(
            "/api/v1/pacient/{}".format(json_data['data']['id']),
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, SUCCESS_CODE)

    
    def test_fetch_pacient_by_id_fail(self):
        """ test fetch pacient by id - FAIL with no existing pacient"""

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_pacient["address"] = json_data["data"]["id"]

        #CREATE PACIENT
        res = self.client().post(
            "/api/v1/pacient/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_pacient),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        #FETCH CREATED PACIENT
        res = self.client().get(
            "/api/v1/pacient/{}".format(10),
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "No pacient found")
        self.assertEqual(res.status_code, ERROR_CODE)

    
    def test_update_pacient_by_id(self):
        """ test update pacient info by id successfully """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_pacient["address"] = json_data["data"]["id"]

        #CREATE PACIENT
        res = self.client().post(
            "/api/v1/pacient/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_pacient),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.update_pacient["pacient_id"] = json_data["data"]["id"]

        #UPDATE PACIENT
        res = self.client().put(
            "/api/v1/pacient/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.update_pacient),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(json_data["data"]['name'], self.update_pacient['name'])
        self.assertEqual(json_data["data"]['surname'], self.update_pacient['surname'])
        self.assertEqual(json_data["data"]['email'], self.update_pacient['email'])
        self.assertEqual(json_data["data"]['phone'], self.update_pacient['phone'])
        self.assertEqual(res.status_code, SUCCESS_CODE)
    
    def test_update_pacient_by_id_fail(self):
        """ test update pacient info by id successfully - FAIL with unexisting pacient """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_pacient["address"] = json_data["data"]["id"]

        #CREATE PACIENT
        res = self.client().post(
            "/api/v1/pacient/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_pacient),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.update_pacient["pacient_id"] = json_data["data"]["id"]+1

        #UPDATE PACIENT
        res = self.client().put(
            "/api/v1/pacient/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.update_pacient),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "No pacient found")
        self.assertEqual(res.status_code, ERROR_CODE)

    
    def test_delete_pacient_by_id(self):
        """ test delete pacient info by id successfully """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_pacient["address"] = json_data["data"]["id"]

        #CREATE PACIENT
        res = self.client().post(
            "/api/v1/pacient/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_pacient),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        #DELETE PACIENT
        res = self.client().delete(
            "/api/v1/pacient/{}".format(json_data["data"]["id"]),
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.update_pacient),
        )
        self.assertEqual(res.status_code, DELETED_CODE)

    
    def test_delete_pacient_by_id_fail(self):
        """ test delete pacient info by id - FAIL with unexisting pacient"""

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_pacient["address"] = json_data["data"]["id"]

        #CREATE PACIENT
        res = self.client().post(
            "/api/v1/pacient/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_pacient),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        #DELETE PACIENT
        res = self.client().delete(
            "/api/v1/pacient/{}".format(json_data["data"]["id"]+1),
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.update_pacient),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "No pacient found")
        self.assertEqual(res.status_code, ERROR_CODE)


    def tearDown(self):
        """ Delete all tables"""
        with self.app.app_context():
            db.session.remove()
            db.drop_all()


if __name__ == "__main__":
    unittest.main()
