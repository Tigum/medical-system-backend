import unittest
import os
import json
from app import create_app, db
from ..utils.CustomResponse import ERROR_CODE, SUCCESS_CODE, CREATION_CODE, DELETED_CODE


class UsersTest(unittest.TestCase):
    def setUp(self):

        self.app = create_app("testing")
        self.client = self.app.test_client
        self.address = {
            "street_name": 'Rua X',
            "zipcode": '12345-67',
            "number": '100',
            "city": 'Campinas',
            "state": 'São Paulo',
            "country": 'Brasil'
        }
        self.create_user = {
            "name": "Pacient name",
            "surname": "Pacient surname",
            "email": "user@gmail.com",
            "password": "123",
            "phone": "19 00000-0000",
            "role": "master",
            "start_at": "09:00",
            "end_at": "18:00",
            "doctor_type": 'Pediatra'
        }
        self.update_user = {
            "name": "João",
            "password": "1234",
            "surname": "Silva",
            "email": "joao@gmail.com",
            "phone": "19 00000-0001"
        }
        self.token = {}

        with self.app.app_context():
            # create all tables
            db.create_all()

    def test_user_creation(self):
        """ test user creation successfully """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_user["address"] = json_data["data"]["id"]

        #CREATE USER
        res = self.client().post(
            "/api/v1/user/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

    def test_user_login(self):
        """ test user login successfully """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_user["address"] = json_data["data"]["id"]

        #CREATE USER
        res = self.client().post(
            "/api/v1/user/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        #LOGIN USER
        res = self.client().post(
            "/api/v1/user/login",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, SUCCESS_CODE)
    
    def test_user_login_fail(self):
        """ test user login - FAIL with incorrect credentials """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_user["address"] = json_data["data"]["id"]

        #CREATE USER
        res = self.client().post(
            "/api/v1/user/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        #LOGIN USER
        self.create_user['password'] = '0'
        res = self.client().post(
            "/api/v1/user/login",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "Invalid credentials")
        self.assertEqual(res.status_code, ERROR_CODE)


    def test_user_login_fail_with_unexisting_user(self):
        """ test user login - FAIL with unexisting user """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_user["address"] = json_data["data"]["id"]

        #CREATE USER
        res = self.client().post(
            "/api/v1/user/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        #LOGIN USER
        self.create_user['email'] = 'user2@gmail.com'
        res = self.client().post(
            "/api/v1/user/login",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "User does not exists")
        self.assertEqual(res.status_code, ERROR_CODE)

    def test_fetch_all_users(self):
        """ test fetch all users successfully """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_user["address"] = json_data["data"]["id"]

        #CREATE USER
        res = self.client().post(
            "/api/v1/user/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        #FETCH CREATED USER
        res = self.client().get(
            "/api/v1/user/",
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(len(json_data["data"]), 1)
        self.assertEqual(res.status_code, SUCCESS_CODE)

    def test_fetch_user_by_id(self):
        """ test fetch user by id successfully """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_user["address"] = json_data["data"]["id"]

        #CREATE USER
        res = self.client().post(
            "/api/v1/user/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        #FETCH CREATED USER
        res = self.client().get(
            "/api/v1/user/{}".format(json_data['data']['id']),
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, SUCCESS_CODE)

    
    def test_fetch_user_by_id_fail(self):
        """ test fetch user by id - FAIL with no existing user"""

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_user["address"] = json_data["data"]["id"]

        #CREATE USER
        res = self.client().post(
            "/api/v1/user/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        #FETCH CREATED USER
        res = self.client().get(
            "/api/v1/user/{}".format(10),
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "No user found")
        self.assertEqual(res.status_code, ERROR_CODE)

    
    def test_update_user_by_id(self):
        """ test update user info by id successfully """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_user["address"] = json_data["data"]["id"]

        #CREATE USER
        res = self.client().post(
            "/api/v1/user/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.update_user["user_id"] = json_data["data"]["id"]

        #UPDATE USER
        res = self.client().put(
            "/api/v1/user/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.update_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(json_data["data"]['name'], self.update_user['name'])
        self.assertEqual(json_data["data"]['surname'], self.update_user['surname'])
        self.assertEqual(json_data["data"]['email'], self.update_user['email'])
        self.assertEqual(json_data["data"]['phone'], self.update_user['phone'])
        self.assertEqual(res.status_code, SUCCESS_CODE)
    
    def test_update_user_by_id_fail(self):
        """ test update user info by id - FAIL with unexisting user """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_user["address"] = json_data["data"]["id"]

        #CREATE USER
        res = self.client().post(
            "/api/v1/user/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.update_user["user_id"] = json_data["data"]["id"]+1

        #UPDATE USER
        res = self.client().put(
            "/api/v1/user/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.update_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "No user found")
        self.assertEqual(res.status_code, ERROR_CODE)

    
    def test_delete_user_by_id(self):
        """ test delete user info by id successfully """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_user["address"] = json_data["data"]["id"]

        #CREATE USER
        res = self.client().post(
            "/api/v1/user/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        #DELETE USER
        res = self.client().delete(
            "/api/v1/user/{}".format(json_data["data"]["id"]),
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.update_user),
        )
        self.assertEqual(res.status_code, DELETED_CODE)

    
    def test_delete_user_by_id_fail(self):
        """ test delete user info by id - FAIL with unexisting user"""

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_user["address"] = json_data["data"]["id"]

        #CREATE USER
        res = self.client().post(
            "/api/v1/user/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        #DELETE USER
        res = self.client().delete(
            "/api/v1/user/{}".format(json_data["data"]["id"]+1),
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.update_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "No user found")
        self.assertEqual(res.status_code, ERROR_CODE)


    def test_check_token(self):
        """ test check token successfully """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_user["address"] = json_data["data"]["id"]

        #CREATE USER
        res = self.client().post(
            "/api/v1/user/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)
        self.token['token'] = json_data['data']['token']

        #CHECK TOKEN
        res = self.client().post(
            "/api/v1/user/token",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.token),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, SUCCESS_CODE)
        
    
    def test_check_token_fail(self):
        """ test check token - FAIL with invalid token """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_user["address"] = json_data["data"]["id"]

        #CREATE USER
        res = self.client().post(
            "/api/v1/user/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)
        self.token['token'] = '{}{}'.format(json_data['data']['token'], 'invalid') 

        #CHECK TOKEN
        res = self.client().post(
            "/api/v1/user/token",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.token),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "Token is invalid")
        self.assertEqual(res.status_code, ERROR_CODE)

    def tearDown(self):
        """ Delete all tables"""
        with self.app.app_context():
            db.session.remove()
            db.drop_all()


if __name__ == "__main__":
    unittest.main()
