import unittest
import os
import json
from app import create_app, db
from ..utils.CustomResponse import ERROR_CODE, SUCCESS_CODE, CREATION_CODE, DELETED_CODE


class ServiceStationTest(unittest.TestCase):
    def setUp(self):

        self.app = create_app("testing")
        self.client = self.app.test_client
        self.address = {
            "street_name": 'Rua X',
            "zipcode": '12345-67',
            "number": '100',
            "city": 'Campinas',
            "state": 'São Paulo',
            "country": 'Brasil'
        }

        self.address_02 = {
            "street_name": 'Rua Y',
            "zipcode": '89101-11',
            "number": '101',
            "city": 'Valinhos',
            "state": 'São Paulo',
            "country": 'Brasil'
        }

        self.address_user = {
            "street_name": 'Rua Z',
            "zipcode": '89101-12',
            "number": '103',
            "city": 'Jaguariuna',
            "state": 'São Paulo',
            "country": 'Brasil'
        }

        self.create_service_station = {
            "label": "Hospital Vera Cruz",
            "private": True,
            "station_type": 'hospital'
        }

        self.create_service_station_02 = {
            "label": "Hospital Mario Gati",
            "private": False,
            "station_type": 'hospital'
        }

        self.create_user = {
            "name": "Pacient name",
            "surname": "Pacient surname",
            "email": "user@gmail.com",
            "password": "123",
            "phone": "19 00000-0000",
            "role": "master",
            "start_at": "09:00",
            "end_at": "18:00"
        }

        self.associte_user_station = {}
        with self.app.app_context():
            # create all tables
            db.create_all()

    def test_service_station_creation(self):
        """ test service station creation successfully """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_service_station["address"] = json_data["data"]["id"]


        #CREATE STATION
        res = self.client().post(
            "/api/v1/service_station/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_service_station),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

    def test_service_station_fetch_all(self):
        """ test service station fetch all successfully """

        #CREATE ADDRESS 1
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_service_station["address"] = json_data["data"]["id"]


        #CREATE STATION 1
        res = self.client().post(
            "/api/v1/service_station/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_service_station),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        #CREATE ADDRESS 2
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address_02),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_service_station_02["address"] = json_data["data"]["id"]

        #CREATE STATION 2
        res = self.client().post(
            "/api/v1/service_station/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_service_station_02),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        #FETCH ALL STATIONS
        res = self.client().get(
            "/api/v1/service_station/",
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(len(json_data["data"]), 2)
        self.assertEqual(res.status_code, SUCCESS_CODE)

    
    def test_service_station_fetch_one(self):
        """ test service station fetch one successfully """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_service_station["address"] = json_data["data"]["id"]


        #CREATE STATION 
        res = self.client().post(
            "/api/v1/service_station/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_service_station),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        #FETCH ALL STATIONS
        res = self.client().get(
            "/api/v1/service_station/{}".format(json_data['data']['id']),
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, SUCCESS_CODE)


    def test_service_station_fetch_one_fail(self):
        """ test service station fetch one - FAIL with unexisting station"""

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_service_station["address"] = json_data["data"]["id"]


        #CREATE STATION 
        res = self.client().post(
            "/api/v1/service_station/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_service_station),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        #FETCH ALL STATIONS
        res = self.client().get(
            "/api/v1/service_station/{}".format(json_data['data']['id']+1),
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "Service station ID does not exist")
        self.assertEqual(res.status_code, ERROR_CODE)


    def test_service_station_association_with_user(self):
        """ test service station association with user successfully"""

        #CREATE STATION ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_service_station["address"] = json_data["data"]["id"]


        #CREATE STATION 
        res = self.client().post(
            "/api/v1/service_station/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_service_station),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)
        self.associte_user_station['station_id'] = json_data['data']['id']

        #CREATE USER ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_user["address"] = json_data["data"]["id"]

        #CREATE USER
        res = self.client().post(
            "/api/v1/user/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)
        self.associte_user_station['user_id'] = json_data['data']['id']

        #ASSOCIATE USER TO STATION
        res = self.client().put(
            "/api/v1/service_station/user",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.associte_user_station),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(len(json_data["data"]['users']), 1)
        self.assertEqual(json_data["data"]['users'][0]['name'], self.create_user['name'])
        self.assertEqual(res.status_code, SUCCESS_CODE)


    def test_service_station_dessociation_with_user(self):
        """ test service station dessociation with user successfully"""

        #CREATE STATION ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_service_station["address"] = json_data["data"]["id"]


        #CREATE STATION 
        res = self.client().post(
            "/api/v1/service_station/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_service_station),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)
        self.associte_user_station['station_id'] = json_data['data']['id']

        #CREATE USER ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_user["address"] = json_data["data"]["id"]

        #CREATE USER
        res = self.client().post(
            "/api/v1/user/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_user),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)
        self.associte_user_station['user_id'] = json_data['data']['id']

        #ASSOCIATE USER TO STATION
        res = self.client().put(
            "/api/v1/service_station/user",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.associte_user_station),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(len(json_data["data"]['users']), 1)
        self.assertEqual(json_data["data"]['users'][0]['name'], self.create_user['name'])
        self.assertEqual(res.status_code, SUCCESS_CODE)

        #DESSOCIATE USER TO STATION
        res = self.client().put(
            "/api/v1/service_station/user/remove/{}/{}".format(self.associte_user_station['user_id'], self.associte_user_station['station_id']),
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(len(json_data["data"]['users']), 0)
        self.assertEqual(res.status_code, SUCCESS_CODE)

    def tearDown(self):
        """ Delete all tables"""
        with self.app.app_context():
            db.session.remove()
            db.drop_all()


if __name__ == "__main__":
    unittest.main()
