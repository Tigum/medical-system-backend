import unittest
import os
import json
from app import create_app, db
from ..utils.CustomResponse import ERROR_CODE, SUCCESS_CODE, CREATION_CODE


class AddressesTest(unittest.TestCase):
    def setUp(self):

        self.app = create_app("testing")
        self.client = self.app.test_client
        self.address = {
            "street_name": 'Rua X',
            "zipcode": '12345-67',
            "number": '100',
            "city": 'Campinas',
            "state": 'São Paulo',
            "country": 'Brasil'
        }

        with self.app.app_context():
            # create all tables
            db.create_all()

    def test_address_creation(self):
        """ test address creation successfully """
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)



    def tearDown(self):
        """ Delete all tables"""
        with self.app.app_context():
            db.session.remove()
            db.drop_all()


if __name__ == "__main__":
    unittest.main()
