import unittest
import os
import json
from app import create_app, db
from ..utils.CustomResponse import ERROR_CODE, SUCCESS_CODE, CREATION_CODE, DELETED_CODE


class ExamsTest(unittest.TestCase):
    def setUp(self):

        self.app = create_app("testing")
        self.client = self.app.test_client
        self.address = {
            "street_name": 'Rua X',
            "zipcode": '12345-67',
            "number": '100',
            "city": 'Campinas',
            "state": 'São Paulo',
            "country": 'Brasil'
        }
        self.create_pacient = {
            "name": "Pacient name",
            "surname": "Pacient surname",
            "email": "pacient@gmail.com",
            "phone": "19 00000-0000"
        }
        self.create_exam = {
            "title": "Exame de sangue",
            "description": "Hemograma completo e Colesterol",
            "is_procedure": False
        }

        with self.app.app_context():
            # create all tables
            db.create_all()

    def test_exam_creation(self):
        """ test exam creation successfully """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_pacient["address"] = json_data["data"]["id"]

        #CREATE PACIENT
        res = self.client().post(
            "/api/v1/pacient/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_pacient),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_exam["pacient"] = json_data["data"]["id"]
    
        #CREATE EXAM
        res = self.client().post(
            "/api/v1/exam/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_exam),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)


    def test_fetch_pacient_exams(self):
        """ test fetch pacient's exams successfully """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_pacient["address"] = json_data["data"]["id"]

        #CREATE PACIENT
        res = self.client().post(
            "/api/v1/pacient/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_pacient),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)
        self.create_pacient["id"] = json_data["data"]["id"]
        self.create_exam["pacient"] = json_data["data"]["id"]
    
        #CREATE EXAM
        res = self.client().post(
            "/api/v1/exam/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_exam),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        #FETCH EXAMS
        res = self.client().get(
            "/api/v1/exam/{}".format(self.create_pacient["id"]),
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(len(json_data["data"]), 1)
        self.assertEqual(res.status_code, SUCCESS_CODE)

    
    def test_fetch_pacient_exams_fail(self):
        """ test fetch pacient's exams successfully - FAIL with invalid pacient ID"""

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_pacient["address"] = json_data["data"]["id"]

        #CREATE PACIENT
        res = self.client().post(
            "/api/v1/pacient/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_pacient),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)
        self.create_pacient["id"] = json_data["data"]["id"]
        self.create_exam["pacient"] = json_data["data"]["id"]
    
        #CREATE EXAM
        res = self.client().post(
            "/api/v1/exam/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_exam),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        #FETCH EXAMS
        res = self.client().get(
            "/api/v1/exam/{}".format(self.create_pacient["id"]+1),
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "Pacient does not exist")
        self.assertEqual(res.status_code, ERROR_CODE)


    def test_delete_exam_by_id(self):
        """ test delete exam by id successfully """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_pacient["address"] = json_data["data"]["id"]

        #CREATE PACIENT
        res = self.client().post(
            "/api/v1/pacient/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_pacient),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_exam["pacient"] = json_data["data"]["id"]
    
        #CREATE EXAM
        res = self.client().post(
            "/api/v1/exam/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_exam),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        exam_id = json_data["data"]["id"]

        #FETCH EXAMS
        res = self.client().get(
            "/api/v1/exam/{}".format(exam_id),
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(len(json_data["data"]), 1)
        self.assertEqual(res.status_code, SUCCESS_CODE)

        #DELETE EXAM
        res = self.client().delete(
            "/api/v1/exam/{}".format(exam_id),
            headers={"Content-Type": "application/json"},
        )
        self.assertEqual(res.status_code, DELETED_CODE)

        #REFETCH EXAMS
        res = self.client().get(
            "/api/v1/exam/{}".format(exam_id),
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(len(json_data["data"]), 0)
        self.assertEqual(res.status_code, SUCCESS_CODE)
    
    def test_delete_exam_by_id_fail(self):
        """ test delete exam by id successfully - FAIL with invalid exam ID """

        #CREATE ADDRESS
        res = self.client().post(
            "/api/v1/address/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.address),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_pacient["address"] = json_data["data"]["id"]

        #CREATE PACIENT
        res = self.client().post(
            "/api/v1/pacient/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_pacient),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        self.create_exam["pacient"] = json_data["data"]["id"]
    
        #CREATE EXAM
        res = self.client().post(
            "/api/v1/exam/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_exam),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        exam_id = json_data["data"]["id"]

        #FETCH EXAMS
        res = self.client().get(
            "/api/v1/exam/{}".format(exam_id),
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(len(json_data["data"]), 1)
        self.assertEqual(res.status_code, SUCCESS_CODE)

        #DELETE EXAM
        res = self.client().delete(
            "/api/v1/exam/{}".format(exam_id+1),
            headers={"Content-Type": "application/json"},
        )
        self.assertEqual(res.status_code, ERROR_CODE)

        

    def tearDown(self):
        """ Delete all tables"""
        with self.app.app_context():
            db.session.remove()
            db.drop_all()


if __name__ == "__main__":
    unittest.main()
